import React from 'react';
import Child from './Child'
const ListadoHijos = (props) => {
    return (
        <div className="container bg-success my-5">
            <h1 className="text-white text-center my-3">Presentando en sociedad l@s hij@s</h1>
            <div className="row"> 
                    {props.arrayHijos.map((item,index)=>(
                        <Child dato={item} key={index}/>

                    ))
                    }
                </div>
                
    
        </div>
    );
};

export default ListadoHijos;