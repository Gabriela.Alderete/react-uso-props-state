import React, { Component } from 'react';
//import FirstChild from '../components/FirstChild';
class ParentComponent extends Component {
    
    state={
        datosHijo :{
            nombre:"",
            apellido:"",
            fechaNac:""
        },
        error:false
    };

    handleChange=(e)=>{
        this.setState({
            datosHijo:{
                ...this.state.datosHijo,
                [e.target.name]:e.target.value
            }
        });
    };

    handleSubmit=(e)=>{
        e.preventDefault();
        const{nombre, apellido, fechaNac}=this.state.datosHijo;
        if(nombre===''|| apellido===''||fechaNac===''){
            //alert("introduzca todos los datos");
            this.setState({
                // pongo la variable error en verdadero para indicar que no se han intruducido todos lo datos
                error:true
            })
            return;
        }
        // vuelvo a poner el erro en falso
        this.props.guardarHijo(this.state.datosHijo);
    };
    
 
    render(props) {
        let mensajeError=this.state.error;
        
        return (
            <div>
                <div className="container-fluid p-4">
                <h1 className="text-center">Soy el componente Padre</h1>
                <p className="text-center">Mi padre App.js me dió el nombre {this.props.darNombre}</p>
                <p className="text-center text-warning">Tendré algunos hijos y seré quien les de sus nombres. Pero mi padre será el encargado de presentarlos en sociedad</p>
            </div>
            <div className="container w-50">
                <form onSubmit={this.handleSubmit}>
                    <h3 className="text-center">Creacion de hij@s</h3>
                    {/* alet que indica que debe ingresar todos los valores */}
                    { 
                        mensajeError ? (
                        <div className="alert alert-dismissible alert-warning">    
                        <p className="mb-0 text-center">Debe introducior todos los datos!!</p>
                        </div>
                        ):null
                    }
                    <div className="form-group">
                        <label>Nombre</label>
                        <input className="form-control" type="text" name="nombre" value={this.state.datosHijo.nombre} onChange={this.handleChange}/>
                     </div>   
                    
                    <div className="form-group">
                        <label>Apellido</label>
                        <input className="form-control" type="text" name="apellido" value={this.state.datosHijo.apellido} onChange={this.handleChange}/>
                    </div>
                    <div className="form-group">
                        <label>Fecha de Nacimiento</label>
                        <input className="form-control" type="date" name="fechaNac" value={this.state.datosHijo.fechaNac} onChange={this.handleChange}/>
                        <div className="d-flex justify-content-center my-2">
                            <button type="submit" class="btn btn-primary btn-lg">crear Hij@</button>
                        </div>
                    </div>
                    {console.log(this.props.nombrePadre)}
                        {/* <FirstChild nombreChild={"Soy el primer hijo: Andres Nicolas"}/>
                        <FirstChild nombreChild={"Soy el segundo hijo: Andres Juaquin"}/>
                        <FirstChild nombreChild={"Soy el tercer hijo: Andres Fabricio"}/> */}
                    {console.log(this.state.datosHijo.nombre)}
                </form>
            </div>

            </div>
            
        );
    }
}

export default ParentComponent;