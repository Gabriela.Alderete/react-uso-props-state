import React from 'react';

const FunctionalComponent = (props) => {
    return (
        <div>
                <p>Hola {props.nombre}</p>
                {console.log(props.nombre)}
            
        </div>
    );
};

export default FunctionalComponent;