import React from 'react';

const ChildComponent = (props) => {
    return (
        <div className="col-lg-3 col-md-3 col-sm-6">
            {/* <p>Soy el primer hijo y mi padres me llamó:</p>
            <p>{props.nombreChild}</p>
             {console.log(props.nombreChild)} */}
            
                <div className="card text-white text-center bg-primary mx-2 mb-3">
                    <div className="card-header">HIJ@</div>
                    <div className="card-body">
                        <h5 className="card-title">Nombre y Apellido</h5>
                        <p>{props.dato.nombre} {props.dato.apellido}</p>
                        <h5>Fecha de Nacimiento</h5>
                        <p className="card-text">{props.dato.fechaNac}</p>
                    </div>
                    <div class="card-footer text-muted">
                        <button className="btn btn-dark btn-lg">Borrar</button>
                    </div>
                </div>
        </div>
    );
};

export default ChildComponent;