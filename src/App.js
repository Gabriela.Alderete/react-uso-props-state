import React ,{Component} from 'react';
import './App.css';
import ParentComponent from './components/ParentComponent';
import ListadoHijos from './components/ListadoHijos';
import "./bootstrap.min.css";

class App extends Component {
  state={
      arregloHijos:[]
  }
  // métodos de ciclo de vida//
  componentDidUpdate(){
    localStorage.setItem('hijosKey',JSON.stringify(this.state.arregloHijos));
  }
  componentDidMount(){
    let storageHijosAux=JSON.parse(localStorage.getItem('hijosKey'))
    if(storageHijosAux){
      this.setState({
        arregloHijos:storageHijosAux
      })
    }
  }


  // función que le enviare a parent para que guarde el hijo
  guardarHijo=(datosHijos)=>{
    console.log("en funcion guardar hijo");
    let arregloAux=this.state.arregloHijos;
    arregloAux.push(datosHijos);
    this.setState({
      arregloHijos:arregloAux
    })

  };

  render(){
    return (
      <div>
        <ParentComponent darNombre={"Andres"} guardarHijo={this.guardarHijo}/>
        <ListadoHijos arrayHijos={this.state.arregloHijos}/>
      </div>
    ); 
  }
}

export default App;
